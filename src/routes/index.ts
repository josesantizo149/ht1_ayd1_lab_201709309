import { Request, Response, Router } from 'express';

class IndexRoutes {

    router: Router;

    constructor() {
        this.router = Router()
        this.routes()
    }

    routes() {
        this.router.get('/', (req, res)=>{
            res.json({nombre: 'José Alejandro Santizo Cotto',carnet:'201709309',curso: 'Análisis y diseño 1', MejorAux: 'Manuel De Mata'})
        })
    }
}

const indexRoutes = new IndexRoutes();
indexRoutes.routes()

export default indexRoutes.router