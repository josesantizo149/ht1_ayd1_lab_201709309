import express from 'express';
import indexRoutes from './routes/index';
;


class Server {

    public app: express.Application;

    constructor() {
        this.app = express()
        this.config()
        this.routes()
    }

    config(){
        this.app.set('port', process.env.PORT || 3100)
    }

    routes(){
        this.app.use(indexRoutes)
    }

    start(){
        this.app.listen(this.app.get('port'), ()=>{
            console.log('Server on port', this.app.get('port'));
        })
    }
}

const server = new Server()
server.start()